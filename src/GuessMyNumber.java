import java.util.Scanner;

public class GuessMyNumber {
    public void Execute (Scanner sc) {
        System.out.println("Choose number from 1 to 100 and press enter");
        sc.nextLine(); // enter pressed

        int start = 1;
        int end = 101;

        String answer = "";
        while (true) {
            int guess = (start + end) / 2;

            boolean valid = false;
            while (valid == false) {
                System.out.println("Is number that you guess is " + guess + "? Type Y or N");
                answer = sc.nextLine();
                valid = validator(answer);
            }

            if (answer.equals("Y") || answer.equals("y")) {
                break;
            }

            valid = false;
            while (!valid) {
                System.out.println("is your number bigger then " + guess + "? Type Y or N");
                answer = sc.nextLine();
                valid = validator(answer);
            }

            if (answer.equals("Y") || answer.equals("y")) {
                start = guess;
            } else {
                end = guess;
            }
        }
        System.out.println("I did it!");
    }


    public static boolean validator(String str) {
        if (str.equals("Y") || str.equals("y") || str.equals("N") || str.equals("n")) {
            return true;
        }
        System.out.println("Please type only Y or N");
        return false;
    }
}

