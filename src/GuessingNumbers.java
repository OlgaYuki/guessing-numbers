import java.util.Scanner;

public class GuessingNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Press 1 if you want to guess my number or press any other key if you want me to try to guess yours");

            String userChoice = sc.nextLine();
            WhatNumber whatNumber = new WhatNumber();
            GuessMyNumber guessMyNumber = new GuessMyNumber();
            if (userChoice.equals("1")) {
                whatNumber.Execute(sc);
            } else {
                guessMyNumber.Execute(sc);
            }
            System.out.println("Do you want to try again? Y/N");
            userChoice = sc.nextLine();

            if (userChoice.equals("N") || userChoice.equals("n")) {
                break;
            }
        }
        System.out.println("Thank you! Goodbye!");
    }
}

