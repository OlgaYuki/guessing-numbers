import java.util.Scanner;

public class WhatNumber {
    public void Execute(Scanner sc) {
        double t = Math.random();
        int unknownNumber = (int) (t * 100) + 1;

        System.out.println("My number is from 1 to 100, guess it and press enter");

        String userInput = sc.nextLine();
        boolean valid = validator(userInput);
        while (valid == false) {
            userInput = sc.nextLine();
            valid = validator(userInput);
        }

        int userNumber = Integer.parseInt(userInput);
        while (true) {
            if (unknownNumber == userNumber) {
                System.out.println("Yes, correct guess!");
                break;
            }
            if (unknownNumber < userNumber) {
                System.out.println("No, my number is less than " + userNumber + ", guess it and press enter");
            } else {
                System.out.println("No, my number is greater than " + userNumber + ", guess it and press enter");
            }
            userNumber = sc.nextInt();
        }
    }

    public static boolean validator(String input) {
        try {
            int userNumber = Integer.parseInt(input);

            if (userNumber >= 1 && userNumber <= 100) {
                return true;
            }
            System.out.println("Please enter the NUMBER from 1 to 100 and try again :)");
            return false;

        } catch (Exception e) {
            System.out.println("Please enter the NUMBER from 1 to 100 and try again :)");
            return false;
        }


    }


}
